# ColorHime (On Progress)
Color Hime aim to create a online Figure/Gundam/FAG hobby paint(e.g. Mr.Color) mixing simulator (base on CMYK). 
Finally, user can choose from CMYK code or CMYK preset from real hobby paint (eg: Mr.Color's product) mix their color to a "fomula", then modify, export and share to their friends.

カラー姫は、フィギュア塗料の混ざるのシミュレーターです。ユーザーはCMYKコードまたは実在の塗料(Mr.カラー)でのプリセットから選んて混ざることをシミュレートします。また、ユーザーは作成した「フォミュラ」シェアしたり、編集したり、エクスポートことができま。

Color Hime係模型嘅顏料混合模型器，基於CMYK顏色混合系統運作。用戶可以通過手動輸入CMYK碼或從我哋嘅來自真實顏色產品嘅Preset顏色中選擇並混合，之後可以吧混合好後嘅配方分享或儲存起來，方便日後參考或再運用。

# early preview 
[http://colorhime.evol.studio/](http://colorhime.evol.studio/)

# Develop Milestone for first release 
- [x] basic CMYK Mixing 
- [x] Export/Import Formula 
- [ ] Select from presets
- [ ] Mr.Color Paint's CMYK code
- [ ] Share to SNS 

# Licenses 
GPLv3, for the details, please check the "LICENSE" file.
