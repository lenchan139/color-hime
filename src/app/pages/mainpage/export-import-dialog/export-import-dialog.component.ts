import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { inputCmyk } from '../../../class/InputCmyk';

@Component({
  selector: 'app-export-import-dialog',
  templateUrl: './export-import-dialog.component.html',
  styleUrls: ['./export-import-dialog.component.less']
})
export class ExportImportDialogComponent implements OnInit {
  colors : Array<inputCmyk> = []
  strColors : string = ""
  constructor(
    @Inject(MAT_DIALOG_DATA) data : any,
    public dialogRef : MatDialogRef<ExportImportDialogComponent>
  ) {
    if (data && data.colors && data.colors.length){
      this.colors = data.colors
      this.strColors = JSON.stringify(this.colors)
    }
   }

  ngOnInit(): void {
  }
  onStringColorChange(s:string ){
    console.log(s)
    this.strColors = s
  }
  tryImport(){
    console.log(this.strColors)
    if(this.strColors){
      try{
        const s = <Array<inputCmyk>> JSON.parse(this.strColors)
        this.colors = s
        if(this.colors.length > 0){
        this.dialogRef.close(this.colors)
        alert('Import success.')
        }else{
          alert('Import Failed. Zero row of color.')
        }
      }catch(e){
        console.error(e)
        alert(`Importing got error.\nTrace Message:\n ${e}`)
      }
    }else{
      alert('JSON Data is empty.')
    }
  }
  closeThis(){
    this.dialogRef.close()
  }
}
