import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportImportDialogComponent } from './export-import-dialog.component';

describe('ExportImportDialogComponent', () => {
  let component: ExportImportDialogComponent;
  let fixture: ComponentFixture<ExportImportDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExportImportDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportImportDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
