import { Component, OnInit } from '@angular/core';
import * as Color from 'color'
import { MatDialog } from '@angular/material/dialog';
import { ExportImportDialogComponent } from './export-import-dialog/export-import-dialog.component';
import { inputCmyk } from '../../class/InputCmyk';
@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.less']
})
export class MainpageComponent implements OnInit {

  // color  = new Color({c:500, m:100, y:0,k:0})
  get mixedColor() {
    let c = 0
    let m = 0
    let y = 0
    let k = 0
    let quantity = 0
    for (const o of this.arrayCmykColors) {
      // console.log(o.cyan)
      quantity = quantity + (o.quantity || 0)
      if (o.quantity > 0) {
        c = c + ((o.cyan || 0) * o.quantity)
        m = m + ((o.magenta || 0) * o.quantity)
        y = y + ((o.yellow || 0) * o.quantity)
        k = k + ((o.black || 0) * o.quantity)
      }
    }
    const r = new Color({
      c: parseFloat((c / quantity).toString()),
      m: parseFloat((m / quantity).toString()),
      y: parseFloat((y / quantity).toString()),
      k: parseFloat((k / quantity).toString())
    })
    // console.log(c)
    return r
  }
  arrayCmykColors: Array<inputCmyk> = []
  constructor(
    private dialog:MatDialog
  ) {
    // console.log(this.mixedColor.hex())
  }

  ngOnInit(): void {
  }
  addLine() {
    this.arrayCmykColors.push({
      name: "New Color",
      cyan: 0,
      magenta: 0,
      yellow: 0,
      black: 0,
      quantity: 1
    })
  }
  openExportImportDialog(){
    const dialogRef = this.dialog.open(ExportImportDialogComponent, {
      data:{
        colors: this.arrayCmykColors
      }
    })
    dialogRef.afterClosed().subscribe(d=>{
      if(d && d.length){
        try{
          this.arrayCmykColors = d
        }catch(e){
          console.error(e)
        }
      }
    })
  }
}
