
export interface inputCmyk {
  name: string,
  cyan: number,
  magenta: number,
  yellow: number,
  black: number,
  quantity: number
}
